package com.gb.pcmanagementapi.repository;

import com.gb.pcmanagementapi.entity.PcManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcManagementRepository extends JpaRepository<PcManagement, Long> {
}
