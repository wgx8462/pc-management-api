package com.gb.pcmanagementapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RunStatus {
    NORMAL("정상"),
    UNDER_INSPECTION("점검중"),
    INSPECTION_REQUIRED("점검필요");

    private final String name;
}
