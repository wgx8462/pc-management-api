package com.gb.pcmanagementapi.entity;

import com.gb.pcmanagementapi.enums.InspectionType;
import com.gb.pcmanagementapi.enums.RunStatus;
import com.gb.pcmanagementapi.enums.UpdateType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class PcManagement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 10)
    private Short seat;
    @Column
    private LocalDate dateUpdate;
    @Column(length = 20)
    @Enumerated(value = EnumType.STRING)
    private UpdateType updateType;
    @Column
    private LocalDate dateCheck;
    @Column(length = 20)
    @Enumerated(value = EnumType.STRING)
    private InspectionType inspectionType;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus;
    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
