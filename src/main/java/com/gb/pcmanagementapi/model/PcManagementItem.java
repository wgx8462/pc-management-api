package com.gb.pcmanagementapi.model;

import com.gb.pcmanagementapi.enums.InspectionType;
import com.gb.pcmanagementapi.enums.RunStatus;
import com.gb.pcmanagementapi.enums.UpdateType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementItem {
    private Long id;
    private Short seat;
    private LocalDate dateUpdate;
    private String updateType;
    private LocalDate dateCheck;
    private String inspectionType;
    private String runStatus;
}
