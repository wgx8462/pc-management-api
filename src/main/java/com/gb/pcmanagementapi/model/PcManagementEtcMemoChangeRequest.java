package com.gb.pcmanagementapi.model;

import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcManagementEtcMemoChangeRequest {
    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
