package com.gb.pcmanagementapi.model;

import com.gb.pcmanagementapi.enums.InspectionType;
import com.gb.pcmanagementapi.enums.RunStatus;
import com.gb.pcmanagementapi.enums.UpdateType;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcManagementRequest {
    private Short seat;

    @Enumerated(value = EnumType.STRING)
    private UpdateType updateType;

    @Enumerated(value = EnumType.STRING)
    private InspectionType inspectionType;

    @Enumerated(value = EnumType.STRING)
    private RunStatus runStatus;

    private String etcMemo;
}
