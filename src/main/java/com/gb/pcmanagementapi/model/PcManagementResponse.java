package com.gb.pcmanagementapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcManagementResponse {
    private Long id;
    private Short seat;
    private LocalDate dateUpdate;
    private String updateTypeName;
    private LocalDate dateCheck;
    private String inspectionTypeName;
    private String runStatusName;
    private String etcMemo;
}
