package com.gb.pcmanagementapi.controller;

import com.gb.pcmanagementapi.entity.PcManagement;
import com.gb.pcmanagementapi.model.PcManagementEtcMemoChangeRequest;
import com.gb.pcmanagementapi.model.PcManagementItem;
import com.gb.pcmanagementapi.model.PcManagementRequest;
import com.gb.pcmanagementapi.model.PcManagementResponse;
import com.gb.pcmanagementapi.service.PcManagementService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/pcManagement")
public class PcManagementController {
    private final PcManagementService pcManagementService;

    @PostMapping("/new")
    public String setPcManagement(@RequestBody PcManagementRequest request) {
        pcManagementService.setPcManagement(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<PcManagementItem> getPcManagements() {
        return pcManagementService.getPcManagements();
    }

    @GetMapping("/detail/{id}")
    public PcManagementResponse getPcManagement(@PathVariable long id) {
        return pcManagementService.getPcManagement(id);
    }

    @PutMapping("/etc-memo/{id}")
    public String putPcManagementEtcMemo(@PathVariable long id, @RequestBody PcManagementEtcMemoChangeRequest request) {
        pcManagementService.putPcManagementEtcMemo(id, request);

        return "OK";
    }
}
