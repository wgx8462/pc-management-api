package com.gb.pcmanagementapi.service;

import com.gb.pcmanagementapi.entity.PcManagement;
import com.gb.pcmanagementapi.model.PcManagementEtcMemoChangeRequest;
import com.gb.pcmanagementapi.model.PcManagementItem;
import com.gb.pcmanagementapi.model.PcManagementRequest;
import com.gb.pcmanagementapi.model.PcManagementResponse;
import com.gb.pcmanagementapi.repository.PcManagementRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcManagementService {
    private final PcManagementRepository pcManagementRepository;

    public void setPcManagement(PcManagementRequest request) {
        PcManagement addData = new PcManagement();
        addData.setSeat(request.getSeat());
        addData.setDateUpdate(LocalDate.now());
        addData.setUpdateType(request.getUpdateType());
        addData.setDateCheck(LocalDate.now());
        addData.setInspectionType(request.getInspectionType());
        addData.setRunStatus(request.getRunStatus());
        addData.setEtcMemo(request.getEtcMemo());

        pcManagementRepository.save(addData);
    }

    public List<PcManagementItem> getPcManagements() {
        List<PcManagement> originList = pcManagementRepository.findAll();

        List<PcManagementItem> result = new LinkedList<>();

        for (PcManagement pcManagement : originList) {
            PcManagementItem addItem = new PcManagementItem();
            addItem.setId(pcManagement.getId());
            addItem.setSeat(pcManagement.getSeat());
            addItem.setDateUpdate(pcManagement.getDateUpdate());
            addItem.setUpdateType(pcManagement.getUpdateType().getName());
            addItem.setDateCheck(pcManagement.getDateCheck());
            addItem.setInspectionType(pcManagement.getInspectionType().getName());
            addItem.setRunStatus(pcManagement.getRunStatus().getName());

            result.add(addItem);
        }

        return result;

    }

    public PcManagementResponse getPcManagement(long id) {
        PcManagement originData = pcManagementRepository.findById(id).orElseThrow();

        PcManagementResponse response = new PcManagementResponse();
        response.setId(originData.getId());
        response.setSeat(originData.getSeat());
        response.setDateUpdate(originData.getDateUpdate());
        response.setUpdateTypeName(originData.getUpdateType().getName());
        response.setDateCheck(originData.getDateCheck());
        response.setInspectionTypeName(originData.getInspectionType().getName());
        response.setRunStatusName(originData.getRunStatus().getName());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putPcManagementEtcMemo(long id, PcManagementEtcMemoChangeRequest request) {
        PcManagement addData = pcManagementRepository.findById(id).orElseThrow();
        addData.setEtcMemo(request.getEtcMemo());

        pcManagementRepository.save(addData);
    }
}
